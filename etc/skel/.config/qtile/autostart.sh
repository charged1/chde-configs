#!/bin/sh

picom &
volumeicon &
xfce4-clipman &
nm-applet &
nitrogen --restore &
mpv --no-video ~/Music/startup.mp3 &
emacs --daemon &
lxsession &
